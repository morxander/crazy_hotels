FROM ruby:2.5.0
RUN apt-get update -qq && \
    apt-get install -y build-essential libpq-dev nodejs
RUN mkdir -p /crazy_hotels/vendor/gems
WORKDIR /crazy_hotels
COPY Gemfile Gemfile.lock /crazy_hotels/
RUN bundle config git.allow_insecure true && \
    bundle install
COPY . /crazy_hotels
RUN bundle exec rake db:migrate
RUN bundle exec rake db:seed