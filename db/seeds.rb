# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Hotel.create(name: 'Alex Hotel', hotel_rate: 5, hotel_fare: 155.99,
             room_amenities: 'Gym, TV, WIFI', city_code: 'ALEX',
             available_from: Time.now, available_to: Time.now + 1.month,
             number_of_adults: 2, discount: '10%')

Hotel.create(name: 'Royal Hotel', hotel_rate: 4, hotel_fare: 120.99,
             room_amenities: 'Gym, TV', city_code: 'ALEX',
             available_from: Time.now + 1.month, available_to: Time.now + 2.month,
             number_of_adults: 2, discount: '15%')

Hotel.create(name: 'Princess Hotel', hotel_rate: 3, hotel_fare: 100.99,
             room_amenities: 'TV', city_code: 'ALEX',
             available_from: Time.now + 1.month, available_to: Time.now + 3.month,
             number_of_adults: 1, discount: '')

Hotel.create(name: 'Al Shaikh Hotel', hotel_rate: 5, hotel_fare: 500.99,
             room_amenities: 'Gym, TV, WIFI, Breakfast, View', city_code: 'DUBAI',
             available_from: Time.now, available_to: Time.now + 4.month,
             number_of_adults: 1, discount: '5%')

Hotel.create(name: 'DAMAC Hotel', hotel_rate: 4, hotel_fare: 200.99,
             room_amenities: 'Gym, TV, WIFI, Breakfast', city_code: 'DUBAI',
             available_from: Time.now, available_to: Time.now + 3.month,
             number_of_adults: 2, discount: '10%')

Hotel.create(name: 'Dubai Hotel', hotel_rate: 3, hotel_fare: 100.55,
             room_amenities: 'Gym, TV', city_code: 'DUBAI',
             available_from: Time.now, available_to: Time.now + 4.month,
             number_of_adults: 1, discount: '')
