class CreateHotels < ActiveRecord::Migration[5.2]
  def change
    create_table :hotels do |t|
      t.string :name
      t.integer :hotel_rate
      t.decimal :hotel_fare
      t.string :room_amenities
      t.string :city_code
      t.datetime :available_from
      t.datetime :available_to
      t.integer :number_of_adults

      t.timestamps
    end
    add_index :hotels, :city_code
    add_index :hotels, :available_from
    add_index :hotels, :available_to
    add_index :hotels, :number_of_adults
  end
end
