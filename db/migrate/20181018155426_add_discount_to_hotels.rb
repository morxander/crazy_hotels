class AddDiscountToHotels < ActiveRecord::Migration[5.2]
  def change
    add_column :hotels, :discount, :string
  end
end
