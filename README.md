# Crazy Hotels API

This is a mock API using **Ruby on Rails** and **SQLite**.

# How to run the project

## Docker

If you want to work with **Docker** just navigate to the project dir in your terminal and execute the following :

```sh
docker-compose up
```

And wait for it to download and prepare everything then try the project in your browser [http://localhost:3002](http://localhost:3002)

## Withour Docker

Install **ruby 2.5.0** on your system ( I recommend using [RVM](https://rvm.io) ) then navigate to the project dir in your terminal and execute the following :

```sh
gem install bundler
bundle install
bundle exec rake db:migrate
bundle exec rake db:seed
```

Then to start the project :

```sh
bundle exec rails s -p 3001 -b '0.0.0.0'
```

Then try the project in your browser [http://localhost:3002](http://localhost:3002)

## How to run the tests

The project has **unit tests** to test the models and **controller tests** to test the **API Controllers**.

To run the tests using **Docker** :

```sh
docker-compose run cracy_hotels bundle exec rails db:test:prepare
docker-compose run cracy_hotels bundle exec rails test test/models/hotels_test.rb test/controllers/hotels_controller.rb
```

To run the tests without **Docker**, Make sure that the ruby environment is installed on your OS and execute :

```sh
bundle exec rails db:test:prepare
bundle exec rails test test/models/hotels_test.rb test/controllers/hotels_controller.rb
```