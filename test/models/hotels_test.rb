# Testing The Hotel Model
class HotelTest < ActiveSupport::TestCase
  test 'Empty Hotel should not be valid' do
    hotel = Hotel.new
    assert_not hotel.valid?
  end
  test 'Hotel object with minimal values should be valid' do
    hotel = Hotel.new
    hotel.name = 'Test Hotel'
    hotel.hotel_fare = 100.55
    hotel.city_code = 'ALEX'
    hotel.room_amenities = 'Gym, View, Breakfast'
    hotel.available_from = Time.now.to_s
    hotel.available_to = (Time.now + 1.month).to_s
    hotel.number_of_adults = 2
    assert hotel.valid?
  end
end
