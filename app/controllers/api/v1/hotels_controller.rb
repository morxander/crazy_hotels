# The Hotels API Controller
module Api::V1
  class HotelsController < ApplicationController
    # Validate the requires params before the index action
    before_action :validate_params, only: [:index]

    # GET /api/v1/hotels
    # @param [String] city
    # @param [Date] from
    # @param [Date] to
    # @param [Integer] adultsCount
    def index
      # Getting all the hotels after sanitizing the user params
      hotels = Hotel.where(city_code: params[:city])
                    .where('number_of_adults >= ?', params[:adultsCount])
                    .where('available_from >= ? AND ? <= available_to',
                           params[:from], params[:to])
                    .select('name as hotelName,
                             hotel_rate as rate,
                             hotel_fare as price,
                             room_amenities as amenities,
                             discount')
      # Convert the rate into stars instead of numbers
      hotels.map { |hotel| hotel.rate = '*' * hotel.rate }
      # Spliting the room_amenities String to Array
      hotels.map { |hotel| hotel.amenities = hotel.amenities.split(',').map(&:strip) }
      # Render the result as JSON
      render json: hotels.to_json(except: %i[id]).to_s
    end

    # The params validator method
    # TODO: Validate the date params
    def validate_params
      if params[:city].blank? || \
         params[:from].blank? || \
         params[:to].blank? || \
         params[:adultsCount].blank?
        render json: { error: 'Please send the required params' }, status: :bad_request
      end
    end
  end
end
