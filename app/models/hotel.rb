class Hotel < ApplicationRecord
  validates :name, presence: true
  validates :hotel_fare, presence: true
  validates :city_code, presence: true
  validates :room_amenities, presence: true
  validates :available_from, presence: true
  validates :available_to, presence: true
  validates :number_of_adults, presence: true
end
